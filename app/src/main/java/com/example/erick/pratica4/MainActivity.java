package com.example.erick.pratica4;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, frguno.OnFragmentInteractionListener, frgdos.OnFragmentInteractionListener {

        Button frag1, frag2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frag1 = (Button)findViewById(R.id.btn_frg1);
        frag2 = (Button)findViewById(R.id.btn_frg2);
        frag1.setOnClickListener(this);
        frag2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_frg1:
                frguno frangmentouno = new frguno();
                FragmentTransaction transactionuno = getSupportFragmentManager().beginTransaction();
                transactionuno.replace(R.id.contenedor,frangmentouno);
                transactionuno.commit();
                break;
            case R.id.btn_frg2:
                frgdos frangmentodos = new frgdos();
                FragmentTransaction transactiondos = getSupportFragmentManager().beginTransaction();
                transactiondos.replace(R.id.contenedor,frangmentodos);
                transactiondos.commit();
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
